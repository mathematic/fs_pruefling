#!/bin/bash

script=$(readlink -f "$0")
script_dir=$(dirname "$script")

bash "$script_dir/build.sh" "0.0.0"

test_dir="$script_dir/../test"

dotnet fsi "$test_dir/examples.fsx"
dotnet fsi "$test_dir/selftest.fsx"
dotnet fsi "$test_dir/tests.fsx"