#!/bin/bash

version=$1

script=$(readlink -f "$0")
script_dir=$(dirname "$script")

dotnet build --property:Version="$version" "$script_dir/../src/pruefling.fsproj"
dotnet pack --property:Version="$version" "$script_dir/../src/pruefling.fsproj"