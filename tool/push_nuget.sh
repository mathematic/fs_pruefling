#!/bin/bash

# Use as $repo/tool/push_nuget.sh $api_key $version

api_key=$1
version=$2

script=$(readlink -f "$0")
script_dir=$(dirname "$script")

bash "$script_dir/build.sh" "$version"
# See https://docs.microsoft.com/en-us/nuget/quickstart/create-and-publish-a-package-using-the-dotnet-cli
dotnet nuget push "$script_dir/../src/bin/Release/Pruefling.$version.nupkg" --api-key "$api_key" --source https://api.nuget.org/v3/index.json