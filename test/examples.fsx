#r @"../src/bin/Debug/netstandard2.0/pruefling.dll"

open Pruefling.Public

let t1 = Make.normal "Normal execution." (fun _ -> 2) (Assert.equal 2)
Run.test t1 |> Analysis.success

let t2 = Make.exceptional "Exceptional execution." (fun _ -> 2 / 0) Assert.alwaysCorrect
Run.test t2 |> Analysis.success

let assert_pair_equal (a, b) =
  match a = b with
  | true -> AssertionResult.correct
  | false -> AssertionResult.failedBecause <| sprintf "%A <> %A in (%A, %A)" a b a b

let t3 = Make.normal "Custom assertion." (fun _ -> (2, 3)) assert_pair_equal
Run.test t3 |> Analysis.success

exception E of int
  with
  member this.Value = this.Data0

let t4 = Make.exceptional "Get detailled error information." (fun _ -> raise (E 4)) (fun (e : E) -> AssertionResult.failedBecause <| sprintf "E.Value: %i" e.Value)

let tests () =
  seq{ t1; t2; t3; t4 }

() |> tests  |> Run.testsSequentially |> Analysis.summarize

() |> tests |> App.console (Some 2)