#r @"../src/bin/Debug/netstandard2.0/pruefling.dll"

open Pruefling.Public

/// Test wrapping a given test and checking for the wrapped tests success status.
let metaTest (case : Success) name (wrapped : Test) : Test =
  let testFunction () =
    wrapped |> Run.test |> Analysis.success
  Make.normal name testFunction (Assert.equal case)

/// Convenience wrapper for normally returning wrapped test.
let selfTestNormal (case : Success) name (testFunction : unit -> 'a) (assertion : 'a -> AssertionResult) : Test =
  let test = Make.normal name testFunction assertion
  metaTest case name test

/// Convenience wrapper for exception throwing wrapped test.
let selfTestExceptional (case : Success) name (testFunction : unit -> 'a) (assertion : 'e -> AssertionResult) : Test =
  let test = Make.exceptional name testFunction assertion
  metaTest case name test

let tests _ =
  seq {
    selfTestNormal Success.Pass "Handling normal 1"
      (fun _ -> 2)
      (Assert.equal 2)
    selfTestNormal Success.Fail "Handling normal 2"
      (fun _ -> 2)
      (Assert.equal 3)
    selfTestNormal Success.Fail "Handling normal 3"
      (fun _ -> 2 / 0)
      Assert.neverReach
    selfTestExceptional Success.Pass "Handling exceptional 1"
      (fun _ -> 2 / 0)
      Assert.alwaysCorrect
    selfTestExceptional Success.Pass "Handling exceptional 2"
      (fun _ -> 2 / 0)
      (fun (e : System.DivideByZeroException) -> Assert.alwaysCorrect ())
    selfTestExceptional Success.Fail "Handling exceptional 3"
      (fun _ -> 2 / 0)
      (fun (e : System.IndexOutOfRangeException) -> Assert.neverReach e)
    selfTestExceptional Success.Fail "Handling exceptional 4"
      (fun _ -> 2)
      Assert.neverReach
  }

tests () |> App.console (Some 2)