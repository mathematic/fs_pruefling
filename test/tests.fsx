#r @"../src/bin/Debug/netstandard2.0/pruefling.dll"

open Pruefling.Public

let test = Make.normal "A" id Assert.alwaysCorrect
let tests = Array.replicate 10 test
let summary =
  tests
  |> Run.testsInParallel None
  |> Analysis.summarize
Array.replicate 10 summary