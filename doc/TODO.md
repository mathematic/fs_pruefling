# Functionality

## Timing

- track time spent on each test run
  - add to Result
- add up in Summary
  - wall time and total time (different for async execution)
  - expose as properties

## Time limits

- allow to set time limit on Test
- maybe switch to builder interface for Test
- limit single test as well as group of tests in `Run.testsX`
- new Outcome case: `TimeOut`
  - track timeouts in Summary

## GC

- can i even track the allocations caused by a single function call?
  - only with serial testing?

# Doc

## Doc generation

- html (xml is already generated)
- via build script

# Dev

- Switch from `*.sh` to `.fsx` files. As its F# and `dotnet fsi` must already be available to build.
