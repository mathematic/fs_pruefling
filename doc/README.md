README for the F# library Prüfling.

This is a minimal F# library for constructing and running tests.

# Overview

Prüfling is

- A small test library with explicit test generation through regular programming.
- Pure F# with focus on usage by F#.
- .NET Standard without dependencies.
- No attempts at metaprogramming, reflection, annotation or IDE integration.

# Usage

- See [[examples.fsx]](https://gitlab.com/mathematic/fs_pruefling/-/blob/master/test/examples.fsx?ref_type=heads).

# Copyright and license

Copyright 2021-2024 by Hofer-Temmel Christoph and Leitz Felix.

The F# source code excluding comments is licensed under the [[AGPL 3.0]](https://www.gnu.org/licenses/agpl-3.0.en.html).

Markdown document and source code comments are licensed under the [[Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)]](https://creativecommons.org/licenses/by-nc-sa/4.0/).

# Source code and development

- See the [[source repo]](https://gitlab.com/mathematic/fs_pruefling).