/// The module containing the whole API of Prüfling.
module Pruefling.Public

/// The custom result type for test assertions.
[<Sealed>]
type AssertionResult

/// Operations for AssertionResult.
module AssertionResult =
  /// Assertion is correct, nothing more to say.
  val correct : AssertionResult
  /// The reason given for failure has to be an ASCI printable string of maximum length 1000.
  val failedBecause : string -> AssertionResult

/// Standard assertions. An assertion should be a pure function.
module Assert =
  /// Results in a successful test.
  val alwaysCorrect : 'a -> AssertionResult
  /// Results in a failed test.
  val neverReach : 'a -> AssertionResult
  /// Results in a failed test.
  val alwaysFail : 'a -> AssertionResult

  /// Asserts equality.
  val equal<'a when 'a : equality> : 'a -> 'a -> AssertionResult
  /// Asserts inequality.
  val different<'a when 'a : equality> : 'a -> 'a -> AssertionResult
  /// Asserts strict inequality.
  val lessThan<'a when 'a : comparison> : 'a -> 'a -> AssertionResult
  /// Asserts inequality.
  val lessThanEqual<'a when 'a : comparison> : 'a -> 'a -> AssertionResult
  /// Asserts strict inequality.
  val greaterThan<'a when 'a : comparison> : 'a -> 'a -> AssertionResult
  /// Asserts inequality.
  val greaterThanEqual<'a when 'a : comparison> : 'a -> 'a -> AssertionResult
  
  /// Asserts set membership.
  val containedIn<'a when 'a : comparison> : Set<'a> -> 'a -> AssertionResult
  /// Asserts element containment.
  val contains<'a when 'a : comparison> : 'a -> Set<'a> -> AssertionResult
  
  /// Transform a boolean check into an assertion.
  val fromBooleanCondition : ('a -> bool) -> 'a -> AssertionResult

/// Combinators for assertions.
module Combine =
  /// Asserts that both assertions hold.
  val logicalAnd : ('a -> AssertionResult) -> ('a -> AssertionResult) -> 'a -> AssertionResult
  /// Asserts that at least one assertion holds.
  val logicalOr : ('a -> AssertionResult) -> ('a -> AssertionResult) -> 'a -> AssertionResult
  /// Asserts that if the first assertion holds, then the second one, too.
  val logicalThen : ('a -> AssertionResult) -> ('a -> AssertionResult) -> 'a -> AssertionResult
  /// Asserts that all elements fulfil the assertion.
  val all : ('a -> AssertionResult) -> seq<'a> -> AssertionResult
  /// Asserts that at least one element fulfils the assertion.
  val any : ('a -> AssertionResult) -> seq<'a> -> AssertionResult

/// The opaque Test type.
[<Sealed>]
type Test =
  member Name : string

/// Constructors for Test.
module Make =
  /// A test whose test function is expected to return a value.
  val normal<'a> : string -> (unit -> 'a) -> ('a -> AssertionResult) -> Test
  /// A test whose test function is expected to throw an exception.
  val exceptional<'a,'e when 'e :> System.Exception> : string -> (unit -> 'a) -> ('e -> AssertionResult) -> Test

/// The opaque result type of a test run.
[<Sealed>]
type Result

/// Various ways to run one or more tests.
module Run =
  /// Run a single test.
  val test : Test -> Result
  /// Run several tests in parallel with optional parallelization limit.
  val testsInParallel : option<int> -> seq<Test> -> array<Result>
  /// Run tests sequentially.
  val testsSequentially : seq<Test> -> array<Result>

/// Summary contains basic statistics over several test run results.
[<Sealed>]
type Summary =
  member Failed : uint
  member Passed : uint
  /// Invariant: Total = Failed + Passed.
  member Total : uint

  member FailedTests : seq<Result>

type Success =
  | Fail
  | Pass

/// Analyze test run results.
module Analysis =
  /// Converts a Result to a Success.
  val success : Result -> Success
  /// Summarize test results.
  val summarize : seq<Result> -> Summary
  /// Prints a summary.
  val printSummary : Summary -> unit
  /// Combine two summaries. Commutative. Basic operation needed for folds.
  val combine : Summary -> Summary -> Summary
  /// Project result down to details message.
  val details : Result -> string
  /// Project result down to test name.
  val name : Result -> string

/// Full applications.
module App = 
  /// Run tests and print their output and statistics to the standard console output.
  /// Maximal parallelism is an optional parameter.
  val console : option<int> -> seq<Test> -> unit