﻿module Pruefling.Public

type AssertionResult =
  private
  | Correct
  | FailedBecause of string

module AssertionResult =

  let correct = Correct

  let regexForFailureReason =
    System.Text.RegularExpressions.Regex "[ -~]{0,1000}"

  let failedBecause reason =
    match (regexForFailureReason.Match reason).Success with
    | true -> FailedBecause reason
    | false -> failwith <| sprintf "Reason does not match regex \"%s\"." (string regexForFailureReason)

module Assert =

  let alwaysCorrect a =
    Correct

  let neverReach a =
    AssertionResult.failedBecause "Should not reach this point."
    
  let alwaysFail a =
    AssertionResult.failedBecause "This assertion always fails."

  let equal a b =
    match a = b with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A <> %A." a b

  let different a b =
    match a <> b with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A = %A." a b 

  let lessThan a b =
    match b < a with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A >= %A." b a

  let lessThanEqual a b =
    match b <= a with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A > %A." b a

  let greaterThanEqual a b =
    match b >= a with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A < %A." b a

  let greaterThan a b =
    match b > a with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A <= %A." b a

  let containedIn set a =
    match Set.contains a set with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A not element of %A." a set

  let contains a set =
    match Set.contains a set with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "%A not element of %A." a set

  let fromBooleanCondition f a =
    match f a with
    | true -> Correct
    | false -> AssertionResult.failedBecause <| sprintf "f(%A) is false." a

module Combine =

  let logicalAnd a1 a2 x =
    match (a1 x, a2 x) with
    | (Correct, Correct) -> Correct
    | (Correct, FailedBecause r2) -> FailedBecause ("Only " + r2)
    | (FailedBecause r1, Correct) -> FailedBecause ("Only " + r1)
    | (FailedBecause r1, FailedBecause r2) -> FailedBecause ("Both " + r1 + " and " + r2)

  let logicalOr a1 a2 x =
    match (a1 x, a2 x) with
    | (Correct, Correct) -> Correct
    | (Correct, FailedBecause _) -> Correct
    | (FailedBecause _, Correct) -> Correct
    | (FailedBecause r1, FailedBecause r2) -> FailedBecause ("Both " + r1 + " and " + r2)
    
  let logicalThen a1 a2 x =
    match (a1 x, a2 x) with
    | (Correct, Correct) -> Correct
    | (Correct, FailedBecause r2) -> FailedBecause r2
    | (FailedBecause r1, _) -> FailedBecause r1

  let all assertion elements =
    let results = elements |> Seq.map assertion |> Set
    match results = Set.singleton Correct with
    | true -> Correct
    | false -> AssertionResult.failedBecause "There was a failing assertion."

  let any assertion elements =
    let startresult = AssertionResult.failedBecause "Sequence is empty or all assertions failed."
    let combine ar1 ar2 =
      match (ar1, ar2) with
      | (FailedBecause _, FailedBecause _) -> startresult
      | _ -> Correct
    elements
    |> Seq.map assertion
    |> Seq.fold combine startresult
   
// This type encodes the expected result of the function being tested.
// Currently all exceptions must be under a common root, which defaults to System.Exception.
// That is, modelling how to catch disjoint subtrees of the exception hierachy is not possible.
type Control<'a, 'e when 'e :> System.Exception> =
  | Normal of ('a -> AssertionResult)
  | Exceptional of ('e -> AssertionResult)

// A test is encoded as an existential type: ∃'a, 'e. Core<'a, 'e>
// For information about how to encode an existential type in F#, see:
// - The following [blogpost](https://stackoverflow.com/questions/16284680/expressing-existential-types-in-f).
// - The stackexchange [answer](https://stackoverflow.com/a/16285497).
// There are four types involved in this encoding.

// Test encoding part 1: The core of a test before wrapping in an existential.
type Core<'a, 'e when 'e :> System.Exception> =
  {
  Name : string
  Function : unit -> 'a
  Control : Control<'a, 'e>
  }

// Test encoding part 2: The inner wrapper in continuation passing style unwrapping the type parameters and yielding the correct return type.
// Type type is: ∀'a,'e.Core<'a,'e> → 'ret
// This is the interface which every projection from the existential type has to implement at the call site.
type Evaluator<'ret> =
  abstract member Eval<'a, 'e when 'e :> System.Exception> : Core<'a, 'e> -> 'ret

// Test encoding part 3: The outer wrapper in continuation passing style choosing the return type.
// The type is: ∀ 'ret. Evaluator<'ret> -> 'ret = ∀ 'ret. (∀'a,'e.Core<'a,'e> → 'ret) -> 'ret = ∃'a, 'e. Core<'a, 'e>
type Applicator =
  abstract member Apply: Evaluator<'ret> -> 'ret

// This is the helper to transform a universally quantified Core into an existentially typed Applicator.
let makeApplicator core =
  { new Applicator with 
    member _.Apply evaluator =
      evaluator.Eval core
  }

// Test encoding part 4: Wrapping an Applicator to not expose an interface type in the signature.
// This ensures that a Test can only be created internally to this module, as Applicator is hidden.
// The extra indirection is the price to pay for full abstraction within F#.
type Test =
  | Test of Applicator

let unwrap (Test applicator) = applicator

module Make =

  let regexForName =
    System.Text.RegularExpressions.Regex "[a-zA-z]([a-zA-z0-9 _]{0,48}[a-zA-z0-9])?"

  let validateName name =
    match (regexForName.Match name).Success with
    | true -> name
    | false -> failwith <| sprintf "Name does not match regex \"%s\"." (string regexForName)

  let normal name f assertion =
    {
    Name = validateName name
    Function = f
    Control = Normal assertion
    } |> makeApplicator |> Test

  let exceptional name f assertion =
    {
    Name = validateName name
    Function = f
    Control = Exceptional assertion
    } |> makeApplicator |> Test

let nameOf (applicator : Applicator) : string =
  applicator.Apply {
    new Evaluator<string> with
     member _.Eval<'a, 'e when 'e :> System.Exception> (core : Core<'a, 'e>) : string =
       core.Name
  }

type Test with
  member this.Name = 
    match this with
    | Test applicator -> nameOf applicator

type Outcome =
  | AsExpected
  | WrongValue of string * obj
  | WrongException of string * System.Exception
  | ValueInsteadOfException of obj
  | ExceptionInsteadOfValue of System.Exception
  | ExceptionOfUnexpectedType of System.Exception

type Result =
  {
  Name : string
  Outcome : Outcome
  }

let runFunction (applicator : Applicator) : Outcome =
  applicator.Apply {
    new Evaluator<Outcome> with
     member _.Eval<'a, 'e when 'e :> System.Exception> (core : Core<'a, 'e>) : Outcome =
       match core.Control with
       | Normal assertion ->
         try
           let result = core.Function ()
           match assertion result with
           | Correct -> AsExpected
           | FailedBecause reason -> WrongValue (reason, result)
         with
         | ex -> ExceptionInsteadOfValue ex
       | Exceptional assertion ->
         try
           let result = core.Function ()
           ValueInsteadOfException result
         with
         | :? 'e as ex ->
           match assertion ex with
           | Correct -> AsExpected
           | FailedBecause reason -> WrongException (reason, ex)
         | ex -> ExceptionOfUnexpectedType ex
  }

let runApplicator (applicator : Applicator)  =
  applicator.Apply {
    new Evaluator<Result> with
     member _.Eval _ =
      {
      Name = nameOf applicator
      Outcome = runFunction applicator
      }
  }

let runApplicatorAsynchronously applicator =
    async { return runApplicator applicator }

let runApplicatorsInParallel (parallelLimit : option<int>) (applicators : seq<Applicator>) =
  let asyncInParallel computations =
    match parallelLimit with
    | None -> Async.Parallel(computations)
    | Some limit -> 
      Async.Parallel(computations, max 1 limit)
  applicators
  |> Seq.map runApplicatorAsynchronously
  |> asyncInParallel
  |> Async.RunSynchronously

module Run =
  let test =
    fun test -> test |> unwrap |> runApplicator
  let testsInParallel optlimit =
    fun tests -> tests |> Seq.map unwrap |> runApplicatorsInParallel optlimit
  let testsSequentially =
    fun tests -> tests |> Seq.map unwrap |> Seq.map runApplicator |> Seq.toArray

type Success =
  | Fail
  | Pass
  with
  override this.ToString () =
    match this with
    | Fail -> "FAIL"
    | Pass -> "PASS"

let outcomeToSuccess outcome =
  match outcome with
  | AsExpected -> Pass
  | _ -> Fail

type Summary =
  {
    _Failed : uint
    _Passed : uint
    _FailedTests : seq<Result>
  }
  with

  member this.Failed = this._Failed

  member this.Passed = this._Passed

  member this.FailedTests = this._FailedTests
  
  member this.Total = this.Failed + this.Passed
    override this.ToString () =
    sprintf "Summary: failed %u | passed %u | total %u"
      this.Failed this.Passed this.Total

module Summary =

  let empty =
    {
      _Failed = 0u
      _Passed = 0u
      _FailedTests = Seq.empty
    }

  let combine (s1 : Summary) (s2 : Summary) =
    {
      _Failed = s1.Failed + s2.Failed
      _Passed = s1.Passed + s2.Passed
      _FailedTests = Seq.append s1.FailedTests s2.FailedTests
    }
  let addResult summary result =
    match result.Outcome with
    | Outcome.AsExpected -> { summary with _Passed = summary.Passed + 1u }
    | _ -> { summary with
                    _Failed = summary.Failed + 1u
                    _FailedTests = Seq.append summary.FailedTests [result]
           }

  let summarize (results : seq<Result>) =
    Seq.fold addResult empty results

  let isSuccessful (summary : Summary) = summary.Failed = 0u

  let show (summary : Summary) =
    if summary.Failed = 0u then
      $"All test passed ({summary.Passed}/{summary.Total}).\n"
    else
      let failedTests =
        summary.FailedTests
        |> Seq.map (fun t -> t.ToString ())
        |> String.concat "\n"
      let numberInfo = $"\n\n{summary.Failed} of {summary.Total} Tests failed.\n"
      failedTests + numberInfo

  let print (summary : Summary) =
    show summary |> System.Console.Write

let resultToSuccess result =
  result.Outcome |> outcomeToSuccess

let summarizeResults results =
  results |> Summary.summarize

let resultDetailLines { Name = name; Outcome = outcome} =
  let header = 
    let success = outcome |> outcomeToSuccess  |> string
    success + " " + name
  let body = 
    let message = 
      match outcome with
      | AsExpected -> "As expected."
      | WrongValue (detail, result) -> 
        $"Returned wrong value. Details: %s{detail}.\nReturned value: %s{result.ToString ()}."
      | WrongException (detail, ex) -> 
        $"Threw wrong exception. Details: %s{detail}.\nType: %s{ex.GetType().ToString()}\n%s{ex.ToString ()}." 
      | ValueInsteadOfException result -> 
        $"Returned value instead of throwing exception.\nReturned value: %s{result.ToString ()}."
      | ExceptionInsteadOfValue ex -> 
        $"Threw exception instead of returning value.\nType: %s{ex.GetType().ToString()}\n%s{ex.ToString ()}."
      | ExceptionOfUnexpectedType ex -> 
        $"Threw exception of unexpected type.\nType: %s{ex.GetType().ToString()}\n%s{ex.ToString ()}."
    let bodyIndent = "   "
    message.Split [| '\n' |]
    |> Seq.map (fun line -> bodyIndent + line)
  seq { header; yield! body }

let summarizeAndDetailResults results =
  seq {
    yield! results |> Seq.map resultDetailLines |> Seq.concat
    yield results |> summarizeResults |> string
  }

module Analysis =
  let success = fun result -> result |> resultToSuccess
  let summarize = fun results -> results |> Summary.summarize
  let printSummary = fun summary -> Summary.print summary
  let combine = fun s1 -> fun s2 -> Summary.combine s1 s2
  let details = fun result -> result |> resultDetailLines |> String.concat ""
  let name = fun result -> result.Name

let runConsole optlimit tests =
  tests
  |> Run.testsInParallel optlimit
  |> summarizeAndDetailResults
  |> Seq.iter (printfn "%s")
  |> ignore

module App = 
  let console optlimit = fun tests -> tests |> runConsole optlimit